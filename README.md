<p align="center">
    <img src="https://codecrocodile.com/images/croc-icon.svg" width="400">
</p>

## Intellicore Pin Generation

As requested this is my solution to the specification provided. 

Please see the [deployed solution](http://pingenerator.codecrocodile.com/) to quickly evaluate the UI.

## Project Setup/Testing
I developed this using the Homestead Vagrant box. If you have VirtualBox installed then: 

- Change the project root mapping in Homestead.yaml
- run "vagrant up"

The app will be running on host: 192.168.10.10

There are a few basic server-side tests that can be run. Obviously you just need to ssh into the vm and run the tests: 

- run "vagrant ssh"
- run "cd code"
- run "php artisan test"

## My Approach

I have chosen to develop a minimal solution but one that I hope demonstrates a solid understanding of software development principles and best practice.

I also choose where possible not to comment my code. I believe code should be self-descriptive otherwise it should be refactored. Also, comments quickly become out-dated and misleading.  

## My Design Decisions

###Performance

Probably the only real major design decision that could be debated is that of the requirement that "program should not repeat a PIN until all the preceding valid PINs have been
emitted". I choose a less performant, but no simpler solution, to enable a demonstration of the SOLID principles I would follow if I were developing a larger application.

My solution generates pin numbers on demand and because of the above requirement it means that the performance will degrade over time. One other obvious solution would be to generate all the pins up front; randomise them; and choose the next least used pin on request.
