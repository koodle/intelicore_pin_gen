<?php

namespace App\Http\Controllers;

use App\Services\PinNumberGeneratorService;
use Illuminate\Http\JsonResponse;

class PinNumberGeneratorController extends Controller
{
    private PinNumberGeneratorService $pinNumberGeneratorService;


    public function __construct(PinNumberGeneratorService $pinNumberGeneratorService)
    {
        $this->pinNumberGeneratorService = $pinNumberGeneratorService;
    }

    public function getNewPinNumber(): JsonResponse
    {
        $pinNumber = $this->pinNumberGeneratorService->generatePinNumber();
        $pinNumberCnt = $this->pinNumberGeneratorService->getPinNumberCount();
        return response()->json([
            'pinNumber' => $pinNumber,
            'pinNumberCnt' => $pinNumberCnt
        ]);
    }
}
