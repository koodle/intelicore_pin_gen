<?php


namespace App\Services\NumberGenerator;


interface NumberGenerator
{
    public function generate(): array;
}
