<?php


namespace App\Services\NumberGenerator;


class SimpleNumberGenerator implements NumberGenerator
{
    public function generate(): array
    {
        return array(random_int(0, 9), random_int(0, 9), random_int(0, 9), random_int(0, 9));
    }
}
