<?php

namespace App\Services;

use App\Services\NumberGenerator\NumberGenerator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PinNumberGeneratorService
{
    public static string $SALT = "20210616_CODECROC_";

    public static function makeHashedPin(string $pinString): string
    {
        /* Note: this only provides minimal protection as MD5 hashing can be brute forced quite easily. At least some extra effort
         * would have to go into reading these for casual db users and anybody else should they be leaked. MD5 consistently returns
         * the same string for the input hence the reason for downgrading the hashing function. */
        return md5(PinNumberGeneratorService::$SALT . $pinString);
    }

    private static int $FOUR_DIGIT_COMBINATION_COUNT = 10 ** 4;

    private NumberGenerator $numberGenerator;


    public function __construct(NumberGenerator $numberGenerator)
    {
        $this->numberGenerator = $numberGenerator;
    }

    public function generatePinNumber(): array
    {
        $numberArray = $this->getNonBlackListedPin();
        $numberArray = $this->getPinNotUsed($numberArray);
        $this->savePin($numberArray);

        return $numberArray;
    }

    public function getPinNumberCount(): int
    {
        return DB::table('pins')->count();
    }

    private function getNonBlackListedPin(): array
    {
        do {
            $numberArray = $this->numberGenerator->generate();
            $pinString = implode($numberArray);
            $pinBlacklisted = $this->isPinBlacklisted($pinString);
        } while ($pinBlacklisted);

        return $numberArray;
    }

    private function getPinNotUsed(array $numberArray): array
    {
        $pinString = implode($numberArray);

        while ($this->isPinUsed($pinString)) {
            $numberArray = $this->numberGenerator->generate();
            $pinString = implode($numberArray);
        }

        return $numberArray;
    }

    private function savePin(array $numberArray): void
    {
        $pinString = implode($numberArray);
        $hashedPinString = $this->makeHashedPin($pinString);
        $nowDate = Carbon::now();
        DB::table('pins')->insert(
            ['pin_hash' => $hashedPinString, 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
    }

    private function isPinBlacklisted(string $pinString): bool
    {
        $cnt = DB::table('blacklistedpins')->where('pin', '=', $pinString)->count();
        return $cnt !== 0;
    }

    private function isPinUsed(string $pinString): bool
    {
        Log::debug("Checking if pin has already been generated: " . $pinString);

        /*
            Performance Note:

            Using this method means the performance will degrade the closer we get to having a total pin count of 10,000 (digits^numbers) i.e
            as we get more hits on numbers that have been blacklisted and already generated.

            One possible solution in future - at least for a small number count - would be to generate all the numbers up front; randomise them; and
            saved them to a database with a usage count. Then when a pin number is requested we just iterate over our records and pick the next
            least used number.
        */

        if ($this->isCanUseDuplicatedPin()) {
            return false;
        } else {
            $hashedPinString = $this->makeHashedPin($pinString);

            Log::debug("Generated hash: " . $hashedPinString);

            $cnt = DB::table('pins')
                ->where('pin_hash', '=', $hashedPinString)
                ->count();

            $cnt > 0 ? Log::debug("Pin used") : Log::debug("Pin not used");

            return $cnt > 0;
        }
    }

    private function isCanUseDuplicatedPin(): bool
    {
        $blackListCnt = DB::table('blacklistedpins')->count();
        $generatedPinCnt = DB::table('pins')->count();
        return $blackListCnt + $generatedPinCnt >= PinNumberGeneratorService::$FOUR_DIGIT_COMBINATION_COUNT;
    }

}

