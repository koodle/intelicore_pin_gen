<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlacklistedPinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nowDate  = Carbon::now();
        DB::table('blacklistedpins')->insert(
            ['pin' => '0000', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '1111', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '2222', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '3333', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '4444', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '5555', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '6666', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '7777', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '8888', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
        DB::table('blacklistedpins')->insert(
            ['pin' => '9999', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );

        DB::table('blacklistedpins')->insert(
            ['pin' => '1234', 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
    }
}
