<?php

use App\Services\PinNumberGeneratorService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nowDate  = Carbon::now();

        $pinString = '1111';
        $pin_hash = PinNumberGeneratorService::makeHashedPin($pinString);

        DB::table('pins')->insert(
            ['pin_hash' => $pin_hash, 'created_at' => $nowDate, 'updated_at' => $nowDate]
        );
    }
}
