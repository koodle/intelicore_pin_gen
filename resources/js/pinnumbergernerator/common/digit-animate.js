export default function (obj, duration, final) {
    const stop = duration / 50;
    const stepTime = Math.abs(Math.floor(duration / stop));
    let current = 0;
    const timer = setInterval(function () {
        current += 1;
        obj.innerHTML = Math.floor(Math.random() * 9);
        if (current == stop) {
            obj.innerHTML = final;
            clearInterval(timer);
        }
    }, stepTime);
}
