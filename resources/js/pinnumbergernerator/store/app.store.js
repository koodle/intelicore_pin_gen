const state = {
    pinNumberCnt: null
};

const getters = {};

const mutations = {
    SET_PIN_NUMBER_COUNT(state, pinNumberCnt) {
        state.pinNumberCnt = pinNumberCnt;
    }
};

const actions = {
    generatePinNumber(context) {
        return new Promise((resolve, reject) => {
            axios.get('/api/new-pin-number')
                .then(function (response) {
                    context.commit('SET_PIN_NUMBER_COUNT', response.data.pinNumberCnt);
                    resolve(response.data);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
