import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import app from './app.store';

export default new Vuex.Store({
  modules: {
    app
  }
});
