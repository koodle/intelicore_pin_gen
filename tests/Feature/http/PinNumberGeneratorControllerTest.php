<?php

namespace Tests\Feature;

use App\Services\PinNumberGeneratorService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class PinNumberGeneratorControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testPinGeneratorReturnsExpectedData(): void
    {
        $pinArray = [1,2,1,2];
        $pinCount = 999;

        $this->instance(PinNumberGeneratorService::class, Mockery::mock(PinNumberGeneratorService::class, function ($mock) use ($pinArray, $pinCount) {
            $mock->shouldReceive('generatePinNumber')->once()->andReturn($pinArray);
            $mock->shouldReceive('getPinNumberCount')->once()->andReturn($pinCount);
        }));

        $response = $this->get('/api/new-pin-number');

        $response->assertStatus(200)
            ->assertExactJson([
                'pinNumber' => $pinArray,
                'pinNumberCnt' => $pinCount
            ]);
    }

    public function testNewPinNumberResponseValid(): void
    {
        $response = $this->get('/api/new-pin-number');
        $response->assertStatus(200);

        $data = $response->decodeResponseJson();
        assert(is_array($data['pinNumber']));
        assert($data['pinNumberCnt'] === 1);
    }

    public function testPinNumberSavedToDb(): void
    {
        $this->get('/api/new-pin-number');
        $this->assertDatabaseCount('pins', 1);
    }

}
