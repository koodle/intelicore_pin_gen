<?php

namespace Tests\Feature;

use App\Services\NumberGenerator\SimpleNumberGenerator;
use App\Services\PinNumberGeneratorService;
use BlacklistedPinsSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use PinsSeeder;
use Tests\TestCase;

class PinNumberGeneratorServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testNumberBlackNotlisted(): void
    {
        $a1 = [1111];

        $sng = Mockery::mock(SimpleNumberGenerator::class);
        $sng->shouldReceive('generate')
            ->andReturn($a1);

        $pngs = new PinNumberGeneratorService($sng);
        $pinArray = $pngs->generatePinNumber();

        self::assertTrue($pinArray === $a1);
    }

    public function testNumberBlacklisted(): void
    {
        $this->seed(BlacklistedPinsSeeder::class);

        $a1 = [1111];
        $a2 = [1212];

        $sng = Mockery::mock(SimpleNumberGenerator::class);
        $sng->shouldReceive('generate')
            ->andReturn($a1, $a2);

        $pngs = new PinNumberGeneratorService($sng);
        $pinArray = $pngs->generatePinNumber();

        self::assertTrue($pinArray === $a2);
    }

    public function testPinNotUsed(): void
    {
        $a1 = [1111];

        $sng = Mockery::mock(SimpleNumberGenerator::class);
        $sng->shouldReceive('generate')
            ->andReturn($a1);

        $pngs = new PinNumberGeneratorService($sng);
        $pinArray = $pngs->generatePinNumber();

        self::assertTrue($pinArray === $a1);
    }

    public function testPinUsed(): void
    {
        $this->seed(PinsSeeder::class);

        $a1 = [1111];
        $a2 = [3232];

        $sng = Mockery::mock(SimpleNumberGenerator::class);
        $sng->shouldReceive('generate')
            ->andReturn($a1, $a2);

        $pngs = new PinNumberGeneratorService($sng);
        $pinArray = $pngs->generatePinNumber();

        self::assertTrue($pinArray === $a2);
    }

}
