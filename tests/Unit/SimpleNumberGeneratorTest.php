<?php

namespace Tests\Unit;

use App\Services\NumberGenerator\SimpleNumberGenerator;
use PHPUnit\Framework\TestCase;

class SimpleNumberGeneratorTest extends TestCase
{
    public function testDigitCountCorrect(): void
    {
        $sng = new SimpleNumberGenerator();
        $pinArray = $sng->generate();
        $this->assertTrue(count($pinArray) === 4);
    }

    public function testDigitsNotNull(): void
    {
        $sng = new SimpleNumberGenerator();
        $pinArray = $sng->generate();
        $this->assertTrue(is_int($pinArray[0]) && is_int($pinArray[1]) && is_int($pinArray[2]) && is_int($pinArray[3]));
    }

    public function testDigitsBetweenZeroAndNine(): void
    {
        $sng = new SimpleNumberGenerator();
        $pinArray = $sng->generate();

        function betweenZereAndNine(int $num) {
            return $num >= 0 && $num <= 9;
        }

        $this->assertTrue(betweenZereAndNine($pinArray[0]) && betweenZereAndNine($pinArray[1]) && betweenZereAndNine($pinArray[2]) && betweenZereAndNine($pinArray[3]));
    }
}
